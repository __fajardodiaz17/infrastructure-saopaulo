output "vpc_id" {
  value       = aws_vpc.infos_vpc.id
  description = "AWS VPC ID"
}

output "public_subnet" {
  value  = aws_subnet.infos_public_subnet.id
  description = "AWS Public Subnet"
}

output "private_subnet" {
  value  = aws_subnet.infos_private_subnet.id
  description = "AWS Private Subnet"
}

output "public_security_group" {
  value       = aws_security_group.infos_vpc_security_group_public.id
  description = "Public security group ID"
}

output "private_security_group" {
  value       = aws_security_group.infos_vpc_security_group_private.id
  description = "Private security group ID"
}


