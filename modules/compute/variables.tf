# 
#               COMPUTE CONFIGURATION
# 

variable "master_type" {
  type        = string
  default     = "c5.xlarge"
  description = "master instance type"
}

variable "slave_type" {
  type        = string
  default     = "c5.2xlarge"
  description = "slave instance type"
}

variable "instance_ami" {
  type        = string
  description = "os for instance"
}

variable "availability_zone" {
  type        = string
  description = "Availability zone"
}

variable "key_name" {
  type        = string
  description = "Key for ssh connection"
}

variable "environment" {
  type        = string
  default     = "development"
  description = "description"
}

variable "master_instance_count" {
  type        = number
  default     = 1
  description = "master instances count"
}

variable "slave_instance_count" {
  type        = number
  default     = 2
  description = "slave instances count"
}


# 
#               NETWORKING CONFIGURATION
# 

# Network for master instance
variable "public_subnet_id" {
  type        = string
  description = "Subnet ID for master instance (public subnet)"
}

variable "public_security_group_id" {
  type        = any
  description = "security group for master instance"
}

# Network for slave instance
variable "private_subnet_id" {
  type        = string
  description = "Subnet ID for slave instance (private subnet)"
}

variable "private_security_group_id" {
  type        = any
  description = "security group for master instance"
}