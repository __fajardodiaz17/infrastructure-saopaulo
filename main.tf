terraform {
  required_version = "~> 1.2"

  backend "s3" {
    bucket = "copacom"
    key    = "saopaulo/terraform.state"
    region = "us-east-1"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~>3.0"
    }
  }
}

provider "aws" {
  region = "sa-east-1"
}

module "networking" {
  source        = "./modules/networking"
  allocation_id = "eipalloc-0aa4912aebf58f99f"
}

module "compute" {
  source = "./modules/compute"

  master_type               = "t2.micro"
  slave_type                = "t2.micro"
  instance_ami              = "ami-00742e66d44c13cd9"
  availability_zone         = "sa-east-1a"
  key_name                  = "AutomatizacionDevopsSaoPaulo"
  environment               = "development"
  public_subnet_id          = module.networking.public_subnet
  public_security_group_id  = [module.networking.public_security_group]
  private_subnet_id         = module.networking.private_subnet
  private_security_group_id = [module.networking.private_security_group]
  master_instance_count     = 1
  slave_instance_count      = 2
}